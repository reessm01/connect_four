////////////////////////
// GLOBAL VARIABLES
////////////////////////

let mode = 0
let enablePlay = true
let turnCounter = 0
const maxTurns = 42
text = document.createTextNode("")
const clickableObjects = document.getElementsByClassName("dropHere")

let playerOne = [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
]

let playerTwo = [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
]

////////////////////////
// DOM PREBUILD
////////////////////////

buildDom()

function buildDom() {

    const destination = document.getElementById("main")
    buildDropLocation(destination)
    buildGameBoard(destination)

    updateTurnDisplay()
    whereToDrop()

}

// Drop location is defined by the intended blocks
// in which the user will be communicating where
// they want their checker to be dropped
// Inputs:: destination to place blocks

function buildDropLocation(destination) {
    for (let i = 0; i < 7; i++) {
        const dropHere = document.createElement("div")
        dropHere.className = "dropHere"
        dropHere.id = "dropHere" + i

        destination.appendChild(dropHere)
    }
}

// Game board is the visual representation of the
// board state.
// Inputs:: destination to place blocks

function buildGameBoard(destination) {
    for (let i = 0; i < 42; i++) {

        const slot = document.createElement("div")
        slot.className = "slot"
        slot.id = "slot" + i

        destination.appendChild(slot)
    }
}

////////////////////////
// INITIALIZE ARRAY FOR PROCESSING
////////////////////////

// Initializes the arrays that will track board state
// The board state array is intentionally transposed to 
// more intuitively "drop" a checker when being iterated.

function whereToDrop() {
    let parent = document.getElementsByClassName("slot")
    arrayCompare = new Array(7) // Intentionally global

    for (let i = 0; i < 7; i++) {
        let start = i;
        arrayCompare[i] = new Array(6)

        initArray(i, start, parent)
    }
}

// Defines the array to properly define board state
// from the DOM.
// Inputs:: interval, starting location & parent (destination)

function initArray(i, start, parent) {
    for (let j = 0; j < 6; j++) {
        for (let j = 0; j < 6; j++) {
            let interval = j * 7;
            arrayCompare[i][j] = start + interval
            let arrayNumber = arrayCompare[i][j]

            fillArrayWithObjects(i, j, arrayNumber, parent)
        }
    }
}

// Interprets and finishes the array by assigning future target
// blocks.

function fillArrayWithObjects(i, j, arrayNumber, parent) {
    for (let child of parent) {
        let number = child.id.replace(/[a-z]/gi, '')
        if (number == arrayNumber) {
            arrayCompare[i][j] = child
            break
        }
    }
}

/////////////////////////////
// USER INTERACTIONS & GAME
/////////////////////////////

for (element of clickableObjects) {
    element.addEventListener('click', connectFour)
}

// This function takes a players input, places a checker
// on a valid move, displays message on invalid moves,
// checks for and displays a winner.

function connectFour(event) {

    if (enablePlay == true && turnCounter <= maxTurns) {
        let dropColumn = event.currentTarget.id.replace(/[a-z]/gi, '')

        let target = event.currentTarget

        let length = arrayCompare[dropColumn].length - 1

        for (let i = length; i >= 0; i--) {

            let parent = arrayCompare[dropColumn][i]
            let hasChild = parent.querySelector(".circle") != null

            if (hasChild == false) {

                placeChecker(parent)
                updateSelection(dropColumn, i)
                victoryConditions()

                if (enablePlay == true) {
                    changeMode()
                    updateTurnDisplay()
                }
                break
            } else columnFullError(parent, hasChild, dropColumn)
        }
    }
    checkForTie()
}

// Places a checker when a valid play is discerned
// Inputs:: parent (destination)

function placeChecker(parent) {
    text.nodeValue = ""
    const circle = document.createElement('div')
    circle.className = "circle"

    selectCheckerColor(circle)

    parent.appendChild(circle)
    turnCounter++
}

// Defines checker color based on mode (player)
// Inputs:: circle (checker)

function selectCheckerColor(circle) {
    if (mode == 0) {
        circle.style.background = "red"
    } else {
        circle.style.background = "black"
    }
}

// Changes the mode (player)

function changeMode() {
    if (mode == 0) {
        mode = 1
    } else {
        mode = 0
    }
}

// Interprets and tracks individual player's moves
// Note: data is transposed from the object array to
// later be more intuitively iterated through.
// Inputs:: selected column & interval

function updateSelection(dropColumn, i) {
    if (mode == 0) {
        playerOne[i][dropColumn] = 1
    } else {
        playerTwo[i][dropColumn] = 1
    }
}

// Updates or intializes the display to visually communicate
// who is next

function updateTurnDisplay() {
    let playersTurnBox = document.getElementById("playersTurn")
    let test = playersTurnBox.querySelector("#circleTracker") != null

    if (test == true) {
        const circle = playersTurnBox.querySelector('#circleTracker')

        selectCheckerColor(circle)
    } else {
        placeNewCheckerTracker(playersTurnBox)
    }
}

// Initializes the turn display
// Inputs:: playersTurnBox (destination)

function placeNewCheckerTracker(playersTurnBox) {
    const circle = document.createElement("div")
    const textBox = document.createElement("div")
    const textDest = document.getElementById("message")
    textBox.appendChild(text)

    circle.id = "circleTracker"
    selectCheckerColor(circle)
    playersTurnBox.appendChild(circle)
    textDest.appendChild(textBox)

}

// Determines an invalid move and updates the message to
// communicate that the column is full.
// Inputs:: parent (destination), hasChild (i.e. contains a checker)
//          & dropColumn (selected column)

function columnFullError(parent, hasChild, dropColumn) {
    if (parent == arrayCompare[dropColumn][0] && hasChild == true) {
        text.nodeValue = "Column is full.\nPlease try again."
    }
}

// Checks if there is a winner

function victoryConditions() {

    if (mode == 0) {
        checkRows(playerOne)
        checkColumns(playerOne)
        checkRHDiag(playerOne)
        checkLHDiag(playerOne)

    } else {
        checkRows(playerTwo)
        checkColumns(playerTwo)
        checkRHDiag(playerTwo)
        checkLHDiag(playerTwo)
    }

}

// Checks the winning condition for 4 subsequent
// matching checkers in a row and displays winner
// if there is one.
// Inputs:: current player

function checkRows(player) {
    let column = player.length - 1
    let row = player[0].length

    for (let i = column; i >= 0; i--) {
        let sum = 0
        for (let j = 0; j < row; j++) {
            if (player[i][j] == 1) {
                sum += 1
                if (sum == 4) {
                    winnerDetected()
                }
            } else {
                sum = 0
            }
        }
    }
}

// Checks the winning condition for 4 subsequent
// matching checkers in a column and displays winner
// if there is one.
// Inputs:: current player

function checkColumns(player) {
    let column = player.length - 1
    let row = player[0].length
    for (let i = 0; i < row; i++) {
        let sum = 0
        for (let j = column; j >= 0; j--) {
            if (player[j][i] == 1) {
                sum += 1
                if (sum == 4) {
                    winnerDetected()
                }
            } else {
                sum = 0
            }
        }
    }
}

// Checks the winning condition for 4 subsequent
// matching checkers in a right hand diagonal and
// displays winner if there is one.
// Inputs:: current player

function checkRHDiag(player) {
    let column = player.length - 1
    let row = player[0].length

    for (let i = column; i >= 0; i--) {
        let sum = 0
        for (let j = 0; j < row; j++) {
            winnerFound = rHDiagCycle(player, i, j, sum)
            if (winnerFound == true) {
                winnerDetected()
            }
        }
    }
}

// Algorithm that cycles through in a right hand 
// diagonal direction
// Inputs:: i (column), j (row) & counted subsequent
//          checkers

function rHDiagCycle(player, i, j, sum) {
    if (i < 0 || j > 6) return false

    if (player[i][j] == 1) {
        sum++
        if (sum == 4) {
            return true
        }
        i--
        j++
        let winnerFound = rHDiagCycle(player, i, j, sum)
        if (winnerFound == true) {
            return true
        } else return false
    } else {
        sum = 0
    }
}

// Checks the winning condition for 4 subsequent
// matching checkers in a left hand diagonal and
// displays winner if there is one.
// Inputs:: current player

function checkLHDiag(player) {
    let column = player.length - 1
    let row = player[0].length

    for (let i = column; i >= 0; i--) {
        let sum = 0
        for (let j = 0; j < row; j++) {
            winnerFound = lHDiagCycle(player, i, j, sum)
            if (winnerFound == true) {
                winnerDetected()
            }
        }
    }
}

// Algorithm that cycles through in a left hand 
// diagonal direction
// Inputs:: i (column), j (row) & counted subsequent
//          checkers

function lHDiagCycle(player, i, j, sum) {
    if (i < 0 || j > 6) return false
    if (player[i][j] == 1) {
        sum++
        if (sum == 4) {
            return true
        }
        i--
        j--
        let winnerFound = lHDiagCycle(player, i, j, sum)
        if (winnerFound == true) {
            return true
        } else return false
    } else {
        sum = 0
    }
}

// Updates message area if a winner is found and
// calls to grey out drop location blocks

function winnerDetected() {
    if (mode == 0) {
        text.nodeValue = "Player One Won!\n\nRefresh page\nto play again."
    } else {
        text.nodeValue = "Player Two Won!\n\nRefresh page\nto play again."
    }
    enablePlay = false
    greyOutDropLocation()
}

// Greys out drop location blocks

function greyOutDropLocation() {
    for (let i = 0; i < 7; i++) {
        let id = "dropHere" + i
        const dropHere = document.getElementById(id)
        dropHere.style.background = "grey"
    }
}

// Checks for and communicates if there is a tie

function checkForTie() {
    if (turnCounter == maxTurns && enablePlay == true) {
        text.nodeValue = "It was a tie.\n\nRefresh page\nto play again."
        greyOutDropLocation()
    }
}